************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                    '(5D19.12)'                                       *
* Copyright (C) 2020, Bruno Tenorio                                    *
************************************************************************

! Print the Dyson orbitals in ASCII format.
! Code adapted from trd_print.f written by P. A. Malmqvist.

      SUBROUTINE DYSAB_PRINT(ISTATE, JSTATE, NDYSAB, DYSAB,
     &                     CMO1, CMO2)
      IMPLICIT REAL*8 (A-H,O-Z)
#include "prgm.fh"
      CHARACTER*16 ROUTINE
      PARAMETER (ROUTINE='DYSAB_PRINT')
#include "rasdim.fh"
!#include "rasdef.fh"
#include "symmul.fh"
#include "rassi.fh"
#include "cntrl.fh"
#include "WrkSpc.fh"
#include "Files.fh"
#include "Struct.fh"
!#include "rassiwfn.fh"
#include "stdalloc.fh"
! Variables passed
      INTEGER ISTATE, JSTATE
      INTEGER NDYSAB
      Real*8 DYSAB(*), CMO1(*), CMO2(*)
      !LOGICAL DO22
! Other variables
      CHARACTER*3 NUM1,NUM2
      CHARACTER*12 FNM
      DIMENSION WBUF(5)
      DIMENSION IOFFA(8)
C IOFFA=NR OF ACTIVE ORBITALS IN PREVIOUS SYMMETRY BLOCKS.
      IOFFA(1)=0
      DO I=1,NSYM-1
        IOFFA(I+1)=IOFFA(I)+NASH(I)
      END DO
! Subroutine starts
      LU=52
      LU=IsFreeUnit(LU)
      WRITE(NUM1,'(I3.3)') ISTATE
      WRITE(NUM2,'(I3.3)') JSTATE
      FNM='DYSO_'//NUM1//'_'//NUM2
      CALL Molcas_Open(LU,FNM)
      WRITE(LU,*)'# Dyson orbital in MO biorth basis from RASSI.'
      WRITE(LU,*)'#  States:'
      WRITE(LU,*) ISTATE, JSTATE
      WRITE(LU,*)'#  Nr of irreps:'
      WRITE(LU,*) NSYM
      WRITE(LU,*)'#  Basis functions:'
      WRITE(LU,'(8I5)') (NBASF(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  Frozen orbitals:'
      WRITE(LU,'(8I5)') (NFRO(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  Inactive orbitals:'
      WRITE(LU,'(8I5)') (NISH(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  Active orbitals:'
      WRITE(LU,'(8I5)') (NASH(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'# total orbitals:'
      WRITE(LU,'(8I5)') (NOSH(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  State ',ISTATE,'    CMO1 coefficients:'
      LPOS=1
      DO ISYM=1,NSYM
        NO=NFRO(ISYM)+NISH(ISYM)+NASH(ISYM)
        NB=NBASF(ISYM)
        DO IO=1,NO
          WRITE(LU,*)'#  Symm ',ISYM,'   Orbital ',IO
          DO i=0,NB-1
          WRITE(LU,'(5E19.12)') CMO1(LPOS+NB*(IO-1)+i)
          END DO
          !WRITE(LU,'(5E19.12)')(CMO1(LPOS+NB*(IO-1)+i),i=0,NB-1)
        END DO
        LPOS=LPOS+NB*NO
      END DO
      WRITE(LU,*)'#  State ',JSTATE,'    CMO2 coefficients:'
      LPOS=1
      DO ISYM=1,NSYM
        NO=NFRO(ISYM)+NISH(ISYM)+NASH(ISYM)
        NB=NBASF(ISYM)
        DO IO=1,NO
          WRITE(LU,*)'#  Symm ',ISYM,'   Orbital ',IO
          DO i=0,NB-1
          WRITE(LU,'(5E19.12)') CMO2(LPOS+NB*(IO-1)+i)
          END DO
          !WRITE(LU,'(5E19.12)')(CMO2(LPOS+NB*(IO-1)+i),i=0,NB-1)
        END DO
        LPOS=LPOS+NB*NO
      END DO

      SYM12=MUL(LSYM1,LSYM2)
      !WRITE(LU,*)'#  States ',ISTATE,JSTATE,' Overlap:'
      !WRITE(LU,'(5D19.12)') SIJ
      WRITE(LU,*)'#  States ',ISTATE,JSTATE,' Dyson orbital from a CI
     & expansion in biorth orbital basis with',NDYSAB,
     &'Symmetry Block elements'
!      LPOS=1
      IOFFTD=0
      DO ISYI=1,NSYM
       NOI=NOSH(ISYI)
       NII=NISH(ISYI)
       IF(NOI.EQ.0) GOTO 600
       WRITE(LU,*)'#sub-Block info:Sym(I),#NumOrb in SymmBlock'
       WRITE(LU,'(8I6)') ISYI,NOI
       DO I=1,NOI
         IA=I+IOFFTD
         write(LU,'(I5,E20.12)') IA,DYSAB(IA)
       END DO
600    CONTINUE
       NORBSYM=NOI
       IOFFTD=IOFFTD+NORBSYM
      END DO 

      CLOSE (LU)
      END SUBROUTINE
