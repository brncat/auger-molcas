************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
*  Copyright (C) 2020, BRUNO TENORIO                                   *
*  PURPOSE: CALCULATE 2-ELECTRON REDUCED TRANSITION DENSITY MATRIX     *
*  FOR CI EXPANSIONS IN BIORTHONORMAL ORBITAL BASES A AND B.           *
************************************************************************
      SUBROUTINE MKRTDM2AB(ISTATE,JSTATE,NRT2M,RT2M,
     &           NRT2MAB,RT2MAB)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION RT2MAB(NRT2MAB)
      DIMENSION RT2M(NRT2M)
#include "Molcas.fh"
#include "cntrl.fh"
#include "rassi.fh"
#include "symmul.fh"
#include "WrkSpc.fh"
!     mktdab(sij,TRAD,TDMAB,iRC)

      DIMENSION IOFFA(8)
C IOFFA=NR OF ACTIVE ORBITALS IN PREVIOUS SYMMETRY BLOCKS.
      IOFFA(1)=0
      DO 5 I=1,NSYM-1
        IOFFA(I+1)=IOFFA(I)+NASH(I)
5     CONTINUE
C  INITIALIZE TRANSITION DENSITY MATRIX:
      CALL FZERO(RT2MAB,NRT2MAB)
C CONTRIBUTION FROM INACTIVE ORBITALS:
C By definition 0 for Dyson orbitals

! it is going to be zero anyway
!        IOFFTD2=0
!        DO 50 ISY=1,NSYM
!         IF(NISH(ISY).NE.0) THEN
!          II=0
!          DO 40 I=1,NISH(ISY)
!            II=II+1
!            IPOS=IOFFTD2+NOSH(ISY)*(II-1)+II
!            RT2MAB(IPOS)=0.0D0
!40        CONTINUE
!          IOFFTD2=IOFFTD2+NOSH(ISY)*(NOSH(ISY)*(NOSH(ISY)+1)/2)
!         END IF
!50      CONTINUE
      write(6,*)'#Size info to help build RT2MZZ'
      WRITE(6,*)'# States ',ISTATE,JSTATE,' 2-e reduced TDM for CI'
     &' Symmetry Block elements #rt2mab:', NRT2MAB
      NRT2MZZ=0
C THEN ADD CONTRIBUTION FROM ACTIVE SPACE.
      ISY12=MUL(LSYM1,LSYM2)
      IOFFTD=0
      DO ISYI=1,NSYM
       NOI=NOSH(ISYI)
       NAI=NASH(ISYI)
       NII=NISH(ISYI)
       NBI=NBASF(ISYI)
       IF(NOI.EQ.0) GOTO 200
       DO ISYJ=1,NSYM
        NOJ=NOSH(ISYJ)
        NAJ=NASH(ISYJ)
        NIJ=NISH(ISYJ)
        NBJ=NBASF(ISYJ)
        IF(NOJ.EQ.0) GOTO 300
        DO ISYL=1,NSYM
         NOL=NOSH(ISYL)
         NAL=NASH(ISYL)
         NIL=NISH(ISYL)
         NBL=NBASF(ISYL)
         IF(NOL.EQ.0) GOTO 400
          IF(MUL(ISYI,MUL(ISYJ,ISYL)).EQ.ISY12) THEN 
            IF(NAI.EQ.0) GOTO 600
            IF(NAJ.EQ.0) GOTO 600
            IF(NAL.EQ.0) GOTO 600
            ! print some block size to help build rt2mzz
            NRT2MZZ=NRT2MZZ+NBI*NBJ*NBL
            WRITE(6,*)'ISYM,JSYM,LSYM'
            WRITE(6,'(3I3)') ISYI,ISYJ,ISYL
            WRITE(6,*)'#Basis Func at Symm-block'
            WRITE(6,'(6I6)')NBI,NBJ,NBL,NBI*NBJ*NBL
            WRITE(6,*)'#Num. Orb. at Symm-block'
            WRITE(6,'(6I6)')NOI,NOJ,NOL,NOI*NOJ*NOL
            DO I=1,NAI
             IA=IOFFA(ISYI)+I
             II=NII+I
             DO J=1,NAJ
              JA=IOFFA(ISYJ)+J
              JJ=NIJ+J
              DO L=1,NAL
              LA=IOFFA(ISYL)+L
              LL=NIL+L
              !II JJ LL are positions in RT2MAB
              !IA JA LA are positions in RT2M
              !IF(JA.GE.LA) THEN
              !IPOS is the position in RT2MAB
              IPOS=IOFFTD+II+NOI*((LL+NOI*(JJ-1))-1)
              !KPOS is the position in RT2M
              KPOS=IA+NASHT*((LA+NASHT*(JA-1))-1)
              RT2MAB(IPOS)=RT2M(KPOS)
              !END IF
              END DO
             END DO
            END DO
             
600         CONTINUE
            NORBSYM=NOI*NOJ*NOL
            IOFFTD=IOFFTD+NORBSYM
          END IF       
400     CONTINUE
        END DO
300    CONTINUE
       END DO
200   CONTINUE
      END DO

      WRITE(6,*)'#Total number of Basis Functions',NRT2MZZ

      RETURN
      END
