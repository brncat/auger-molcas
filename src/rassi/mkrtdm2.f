************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                                                                      *
*    Copyright (C) 2020, Bruno Tenorio                                 *
************************************************************************
      SUBROUTINE MKRTDM2(IFSBTAB1,IFSBTAB2,ISSTAB,
     &                  MAPORB,DET1,DET2,
     &                  IF21,IF12,NRT2M,RT2M,
     &                  ISTATE,JSTATE)
             !          ,job1,job2,ist,jst)

      IMPLICIT NONE
      INTEGER IFSBTAB1(*),IFSBTAB2(*)
      INTEGER ISSTAB(*),MAPORB(*),NRT2M
      REAL*8 DET1(*),DET2(*)
      REAL*8 RT2M(NRT2M)
      INTEGER NASHT,NASORB,LORBTB,IP
!      REAL*8 SGNJL,SGNIK
      REAL*8 GVAL,GAAA,GAAB,GABA,GABB,GBAA,GBAB,GBBA,GBBB
!      !INTEGER LSYM1,MSPOJ1,LSYM2,MSPROJ2,ISYOP,MS2OP
!      !INTEGER MPLET1,MPLET2, MPLETD

      INTEGER IAJBLA,IAJBLB,IBJALA,IBJALB

      INTEGER IAJALA,IAJALB,IBJBLA,IBJBLB,IAJBJA,IBJBJA
      INTEGER LORB,JORB,IORB 
      INTEGER JORBA,JORBB,LORBA,LORBB,IORBA,IORBB
      INTEGER ITABS,JTABS,LTABS,JLTABS,IJLTABS
!      INTEGER IBKA,IBKB,IJ,IJIJ,IORBA,IORBB,ITU,ITUVX
!      INTEGER IVABS,IVX,IXABS,JALA,JALB,JBJA,JBLA,JBLB
!      INTEGER JORBA,JORBB,KORB,KORBA,KORBB,LORB,LORBA,LORBB

      INTEGER NASGEM,NSRT2M,ISTATE,JSTATE,SIGNLJ
      LOGICAL IF21,IF12
      !INTEGER job1,job2,ist,jst
#include "symmul.fh"
#include "stdalloc.fh"
#include "WrkSpc.fh"
      Real*8, Allocatable:: SRT2M(:)

C Given two CI expansions, using a biorthonormal set of SD''s,
C calculate the spin-summed 2-particle transition density matrix
C in the biorthonormal active orbital basis.

      LORBTB=ISSTAB(3)
C Pick out nr of active orbitals from orbital table:
      NASORB=IWORK(LORBTB+3)
      NASHT=NASORB/2
      NASGEM=(NASORB*(NASORB-1))/2
      NSRT2M=NASORB*(NASORB*(NASORB-1))/2
      Call mma_allocate(SRT2M,nSRT2M,Label='SRT2M')
      SRT2M(:)=0.0D0
      !ISYOP   = MUL(LSYM1,LSYM2)
      !MS2OP   = MSPROJ1-MSPROJ2
      !MPLETD =  MPLET1 - MPLET2

        CALL SRTDM2(IWORK(LORBTB),ISSTAB,
     &              IFSBTAB1,IFSBTAB2,DET1,DET2,
     &              IF21,IF12,NSRT2M,SRT2M)
      
C Mapping from active spin-orbital to active orbital in external order.
C Note that these differ, not just because of the existence of two
C spin-orbitals for each orbital, but also because the active orbitals
C (external order) are grouped by symmetry and then RAS space, but the
C spin orbitals are grouped by subpartition.
      IP=0
      IAJALA=0      ! dummy initialize
      IAJALB=0      ! dummy initialize
      IAJBLA=0      ! dummy initialize
      IAJBLB=0      ! dummy initialize
      IBJALA=0      ! dummy initialize
      IBJALB=0      ! dummy initialize
      IBJBLA=0      ! dummy initialize
      IBJBLB=0      ! dummy initialize
      IAJBJA=0      ! dummy initialize for J=L
      IBJBJA=0      ! dummy initialize for J=L

      DO IORB=1,NASHT
       IORBA=2*IORB-1
       IORBB=2*IORB
       ITABS=MAPORB(IORBA)
!       write(6,*)'BRN IORB,ITABS',IORB,ITABS
       DO JORB=1,NASHT
        JORBA=2*JORB-1
        JORBB=2*JORB
        JTABS=MAPORB(JORBA)
!        write(6,*)'BRN JORB,JTABS',JORB,JTABS
        DO LORB=1,NASHT
         LORBA=2*LORB-1
         LORBB=2*LORB
         LTABS=MAPORB(LORBA)
         JLTABS=LTABS+NASHT*(JTABS-1)
!         write(6,*)'BRN LORB,LTABS',LORB,LTABS
         IF(JORB.GT.LORB) THEN
          SIGNLJ=1
          IAJALA=IORBA+NASORB*(((JORBA-1)*(JORBA-2)/2)+LORBA-1)
          IAJALB=IORBA+NASORB*(((JORBA-1)*(JORBA-2)/2)+LORBB-1)
          IAJBLA=IORBA+NASORB*(((JORBB-1)*(JORBB-2)/2)+LORBA-1)
          IAJBLB=IORBA+NASORB*(((JORBB-1)*(JORBB-2)/2)+LORBB-1)
          IBJALA=IORBB+NASORB*(((JORBA-1)*(JORBA-2)/2)+LORBA-1)
          IBJALB=IORBB+NASORB*(((JORBA-1)*(JORBA-2)/2)+LORBB-1)
          IBJBLA=IORBB+NASORB*(((JORBB-1)*(JORBB-2)/2)+LORBA-1)
          IBJBLB=IORBB+NASORB*(((JORBB-1)*(JORBB-2)/2)+LORBB-1)
          GAAA=SRT2M(IAJALA)
          GABA=SRT2M(IAJBLA)
          GABB=SRT2M(IAJBLB)
          !GAAB=SRT2M(IAJALB)
          !GBAA=SRT2M(IBJALA)
          GBAB=SRT2M(IBJALB)
          GBBA=SRT2M(IBJBLA)
          GBBB=SRT2M(IBJBLB)
           GVAL=GAAA+GABA+GBAB+GBBB!+GAAB+GBBA!+GABB+GBAA
!          WRITE(6,*) 'BRN IJL set',IAJALA,IAJALB,IAJBLA,IAJBLB,
!     &             IBJALA,IBJALB,IBJBLA,IBJBLB
         ELSE IF(JORB.EQ.LORB) THEN
          !IAJBLA=IORBA+NASORB*(((JORBB-1)*(JORBB-2)/2)+LORBA-1)
          !IBJBLA=IORBB+NASORB*(((JORBB-1)*(JORBB-2)/2)+LORBA-1)
          IAJBJA=IORBA+NASORB*(((JORBB-1)*(JORBB-2)/2)+JORBA-1)
          IBJBJA=IORBB+NASORB*(((JORBB-1)*(JORBB-2)/2)+JORBA-1)
          GABA=SRT2M(IAJBJA)
          GBBA=SRT2M(IBJBJA)
          GVAL=(GABA-GBBA)
!          WRITE(6,*) 'BRN IJL set',IAJBLA,IBJBLA
         ELSE IF(JORB.LT.LORB) THEN
          SIGNLJ=-1
          IAJALA=IORBA+NASORB*(((LORBA-1)*(LORBA-2)/2)+JORBA-1)
          IAJALB=IORBA+NASORB*(((LORBA-1)*(LORBA-2)/2)+JORBB-1)
          IAJBLA=IORBA+NASORB*(((LORBB-1)*(LORBB-2)/2)+JORBA-1)
          IAJBLB=IORBA+NASORB*(((LORBB-1)*(LORBB-2)/2)+JORBB-1)
          IBJALA=IORBB+NASORB*(((LORBA-1)*(LORBA-2)/2)+JORBA-1)
          IBJALB=IORBB+NASORB*(((LORBA-1)*(LORBA-2)/2)+JORBB-1)
          IBJBLA=IORBB+NASORB*(((LORBB-1)*(LORBB-2)/2)+JORBA-1)
          IBJBLB=IORBB+NASORB*(((LORBB-1)*(LORBB-2)/2)+JORBB-1)
          GAAA=SRT2M(IAJALA)
          GAAB=SRT2M(IAJALB)
          GABA=SRT2M(IAJBLA)
          !GABB=SRT2M(IAJBLB)
          !GBAA=SRT2M(IBJALA)
          GBAB=SRT2M(IBJALB)
          GBBA=SRT2M(IBJBLA)
          GBBB=SRT2M(IBJBLB)
           GVAL=SIGNLJ*(GAAA+GABA+GBAB+GBBB)!+GAAB+GBBA)!+GABB+GBAA
         END IF
         IJLTABS=ITABS+NASHT*(JLTABS-1)
!         write(6,*)'BRN ITABS,JLTABS,IJLTABS',ITABS,JLTABS,IJLTABS
         RT2M(IJLTABS)=GVAL
!         WRITE(6,*) 'BRN RT2M(IJLTABS) numb. steps',IP !,IJLTABS
!  123     CONTINUE
        END DO
       END DO
      END DO

      CALL mma_deallocate(SRT2M)

      !WRITE(6,*) 'BRN WRITE RT2M',NRT2M
      !DO IP=1,NRT2M
      !  WRITE(6,*) IP,RT2M(IP)
      !END DO

      RETURN
      END
