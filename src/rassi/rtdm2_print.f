************************************************************************
* This file is part of OpenMolcas.                                     *
*                                                                      *
* OpenMolcas is free software; you can redistribute it and/or modify   *
* it under the terms of the GNU Lesser General Public License, v. 2.1. *
* OpenMolcas is distributed in the hope that it will be useful, but it *
* is provided "as is" and without any express or implied warranties.   *
* For more details see the full text of the license in the file        *
* LICENSE or in <http://www.gnu.org/licenses/>.                        *
*                    '(5D19.12)'                                       *
* Copyright (C) 2020, Bruno Tenorio                                    *
************************************************************************

! Print the reduced 2-e TDM in ASCII format.
! Code adapted from trd_print.f written by P. A. Malmqvist.

      SUBROUTINE RTDM2_PRINT(ISTATE, JSTATE, NRT2MAB, RT2MAB,
     &                     CMO1, CMO2)
      IMPLICIT REAL*8 (A-H,O-Z)
#include "prgm.fh"
      CHARACTER*16 ROUTINE
      PARAMETER (ROUTINE='RTDM2_PRINT')
#include "rasdim.fh"
!#include "rasdef.fh"
#include "symmul.fh"
#include "rassi.fh"
#include "cntrl.fh"
#include "WrkSpc.fh"
#include "Files.fh"
#include "Struct.fh"
!#include "rassiwfn.fh"
#include "stdalloc.fh"
! Variables passed
      INTEGER ISTATE, JSTATE
      INTEGER NRT2MAB
      Real*8 RT2MAB(*), CMO1(*), CMO2(*)
      !LOGICAL DO22
! Other variables
      CHARACTER*3 NUM1,NUM2
      CHARACTER*12 FNM
      DIMENSION WBUF(5)
      DIMENSION IOFFA(8)
C IOFFA=NR OF ACTIVE ORBITALS IN PREVIOUS SYMMETRY BLOCKS.
      IOFFA(1)=0
      DO I=1,NSYM-1
        IOFFA(I+1)=IOFFA(I)+NASH(I)
      END DO
! Subroutine starts
      LU=51
      LU=IsFreeUnit(LU)
      WRITE(NUM1,'(I3.3)') ISTATE
      WRITE(NUM2,'(I3.3)') JSTATE
      FNM='r2TM_'//NUM1//'_'//NUM2
      CALL Molcas_Open(LU,FNM)
      WRITE(LU,*)'# 2-electron reduced Transition density from RASSI.'
      WRITE(LU,*)'#  States:'
      WRITE(LU,*) ISTATE, JSTATE
      WRITE(LU,*)'#  Nr of irreps:'
      WRITE(LU,*) NSYM
      WRITE(LU,*)'#  Basis functions:'
      WRITE(LU,'(8I5)') (NBASF(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  Frozen orbitals:'
      WRITE(LU,'(8I5)') (NFRO(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  Inactive orbitals:'
      WRITE(LU,'(8I5)') (NISH(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  Active orbitals:'
      WRITE(LU,'(8I5)') (NASH(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'# total orbitals:'
      WRITE(LU,'(8I5)') (NOSH(ISYM),ISYM=1,NSYM)
      WRITE(LU,*)'#  State ',ISTATE,'    CMO coefficients:'
      LPOS=1
      DO ISYM=1,NSYM
        NO=NFRO(ISYM)+NISH(ISYM)+NASH(ISYM)
        NB=NBASF(ISYM)
        DO IO=1,NO
          WRITE(LU,*)'#  Symm ',ISYM,'   Orbital ',IO
          DO i=0,NB-1
          WRITE(LU,'(5E19.12)') CMO1(LPOS+NB*(IO-1)+i)
          END DO
          !WRITE(LU,'(5E19.12)')(CMO1(LPOS+NB*(IO-1)+i),i=0,NB-1)
        END DO
        LPOS=LPOS+NB*NO
      END DO
      WRITE(LU,*)'#  State ',JSTATE,'    CMO coefficients:'
      LPOS=1
      DO ISYM=1,NSYM
        NO=NFRO(ISYM)+NISH(ISYM)+NASH(ISYM)
        NB=NBASF(ISYM)
        DO IO=1,NO
          WRITE(LU,*)'#  Symm ',ISYM,'   Orbital ',IO
          DO i=0,NB-1
          WRITE(LU,'(5E19.12)') CMO2(LPOS+NB*(IO-1)+i)
          END DO
          !WRITE(LU,'(5E19.12)')(CMO2(LPOS+NB*(IO-1)+i),i=0,NB-1)
        END DO
        LPOS=LPOS+NB*NO
      END DO

      SYM12=MUL(LSYM1,LSYM2)
      !WRITE(LU,*)'#  States ',ISTATE,JSTATE,' Overlap:'
      !WRITE(LU,'(5D19.12)') SIJ
      WRITE(LU,*)'#  States ',ISTATE,JSTATE,' 2-e reduced TDM for CI
     & expansion in biorth orbital basis with',NRT2MAB,
     &'Symmetry Block elements'
!      LPOS=1
      IOFFTD=0
      DO ISYI=1,NSYM
       NOI=NOSH(ISYI)
       DO ISYJ=1,NSYM
        NOJ=NOSH(ISYJ)
        DO ISYL=1,NSYM
         NOL=NOSH(ISYL)
          IF(MUL(ISYI,MUL(ISYJ,ISYL)).EQ.SYM12) THEN
            IF(NOI.EQ.0) GOTO 600
            IF(NOJ.EQ.0) GOTO 600
            IF(NOL.EQ.0) GOTO 600
!           > Write out one symmetry block (4 indices!) of
!             two-electron
!           > transition density matrix elements.
            WRITE(LU,*)'#sub-Block info:Sym(I,J,L),#NumOrb in SymmBlock'
            WRITE(LU,'(8I6)') ISYI,ISYJ,ISYL,NOI*NOJ*NOL
            DO I=1,NOI
             IA=IOFFA(ISYI)+I
             DO J=1,NOJ
              JA=IOFFA(ISYJ)+J
              DO L=1,NOL
               LA=IOFFA(ISYL)+L
               !IA,JA,LA are original orb. indexes
               IPOS=IOFFTD+I+NOI*((L+NOI*(J-1))-1)
               write(LU,'(3I5,E20.12)') IA,JA,LA,RT2MAB(IPOS)
              END DO
             END DO
            END DO
600         CONTINUE
            NORBSYM=NOI*NOJ*NOL
            IOFFTD=IOFFTD+NORBSYM
          END IF
        END DO
       END DO
      END DO

      CLOSE (LU)
      END SUBROUTINE
