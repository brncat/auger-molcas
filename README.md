OpenMolcas
==========

This is a modified version of OpenMolcas intended to be a platform for the implementation of Auger spectroscopy.

Bruno Tenorio
Nov. 2020.


A brief description of the Auger project on gitlab.

A few new functions were added to the rassi module of OpenMolcas. They are: 
* srtdm2.f which builds the reduced two-electron transition density in a spin-orbital basis by the application of the annihilation operator once in PSI1 (N-1-electron wave function) and twice in PSI2 (N-electron wave function). It returns a tensor called SRT2M of indices i,j,l for each spin-orbital which was annihilated. The size of SRT2M is NSRT2M=NASORB**2(NASORB-1)/2. Notice that this step can be improved by recognizing the spin projection of the N-1 wave function.
* mkrtdm2.f which collect the tensor SRT2M and build the spin summed density. At the present moment it is summing the components aaa, bbb, aba, and bab. This step can also be improved once we recognize the spin projection of the N-1 wave function. It stores the components in the tensor RT2M of size NASHT**3.
* mkrtdm2ab.f collects the components of RT2M in symmetry blocks. The tensor NRT2MAB of size Sum(i,j,l)NSYM NOSHT(i).NOSHT(j).NOSHT(l) collect the symmetry blocks.
* rtdm2_print.f write the symmetry blocks of NRT2MAB in a file alongside with the CMO1 and CMO2 that contains the biorthonormal orbitals in atomic basis. It writes the files r2TM_X_Y where x is the state number of psi2 and Y the state number of psi1.
* dysab_print.f works the same way for the Dyson orbitals.
* The final step of transforming from the MO basis to the atomic orbital basis (which is necessary to use with the Bspline code) is done in an external python code. The python program is a simple code based on numpy that reads the files r2TM_ (and DYSO_) and performs the tensor.matrix multiplications using einsum.np numpy function.
